(function () {
  "use strict";
  window.app = {
    /**
    * Array that store the stockquotes.
    **/
    stocks: [],

    /**
    * Websocket connected to "http://server7.tezzt.nl:1333".
    **/
    socket : io('http://server7.tezzt.nl:1333'),

    testData : data,

    /**
    * Object that stores setting properties.
    **/
    settings: {
      /**
      * Refresh time in miliseconds.
      **/
      refresh: 1000,
      /**
      * String url for ajax requests.
      **/
      ajaxUrl: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php"
    },

    /**
    * Set the callback of the socket to the "stockquote" messages.
    *
    * @return boolean. If everything went ok, true; if not false.
    *
    **/
    init: function (opt) {
      if(opt === 0){
        app.socket.on("stockquotes",app.realTimeDataCallback);
        return true;
      }else if(opt === 1){
        setInterval(app.fetchStocks(app.settings.ajaxUrl),app.settings.refresh);
        return true;
      }else if(opt === 2){
        app.stocks = data;
        app.interface.createTable(app.stocks);
        return true;
      }else{
        return false;
      }
    },

    /**
    * Receive the data from the socket and update table.
    *
    * It empty the stocks array, erase the table, fill stocks with new
    * stockquotes from data received by the socket.on("stockquotes",callback) and
    * creates the new table.
    *
    * @param data : JSON object from socket.on("stockquotes");
    *
    * @return boolean. If everything went ok, true; if not false.
    *
    **/
    realTimeDataCallback: function(data){
      app.stocks = [];
      app.interface.destroyTable();
      app.stocks = data.query.results.row;
      app.interface.createTable(app.stocks);
      return true;
    },

    /**
    * Fetch the stockquotes using AJAX.
    *
    * It empty the stocks array, erase the table, fill stocks with new
    * stockquotes from data received by the socket.on("stockquotes",callback) and
    * creates the new table.
    *
    * @param url : String url from where to fetch the stockquotes;
    *
    * @return XMLHttpRequest.
    *
    **/
    fetchStocks : function(url){
      var xhr;
      xhr=new XMLHttpRequest();
      xhr.open("GET", url);
      xhr.addEventListener("load", app.retrieveResponse);
      xhr.send();
      return xhr;
    },

    /**
    * Receive the data from the socket and update table.
    *
    * It empty the stocks array, erase the table, fill stocks with new
    * stockquotes from data received by the socket.on("stockquotes",callback) and
    * creates the new table.
    *
    * @param data : JSON object from socket.on("stockquotes");
    *
    * @return boolean. If everything went ok, true; if not false.
    *
    **/
    retrieveResponse: function(e){
      app.interface.destroyTable();
      var stocks = JSON.parse(e.target.responseText).query.results.row;
      app.interface.createTable(stocks);
      return true;
    },

    interface: {
      header : null,
      table : null,

      /**
      * Check if a stock is rising or falling.
      *
      * @param stock : single stock object;
      *
      * @return boolean. True(green) for rising stock and false(red) for falling ones.
      *
      **/
      greenOrRed : function(criteria){
        if(criteria >= 0){
          return true; // True for Green!
        }else{
          return false; // False for Red!
        }
      },

      /**
      * Destroy stock table.
      *
      * @param -;
      *
      * @return boolean. True is just for testing.
      *
      **/
      destroyTable: function(){
        var body = document.getElementsByTagName("body")[0];
        var table = document.getElementById("stockquotes_table");
        if(table){
          body.removeChild(table);
          return true;
        }else{
          return false;
        }
      },

      /**
      * Creates the stock table and fill it with the stock with the right color.
      *
      * @param stocks : array stocks objects that will fill the table;
      *
      * @return boolean. Just for testing.
      *
      **/
      createTable: function(stocks){
        var body = document.getElementsByTagName("body")[0];
        var table = document.createElement("table");
        table.id = "stockquotes_table";
        for(var i = 0; i < stocks.length; i++){
          var row = document.createElement("tr");
          var stock = stocks[i];
          for(var property in stock){
            var col = document.createElement("td");
            col.style.border = '1px black solid';
            col.innerHTML = stock[property];
            if(app.interface.greenOrRed(stock.col4)){
              col.style.color = "green";
            }else{
              col.style.color = "red";
            }
            row.appendChild(col);
          }
          table.appendChild(row);
        }
        body.appendChild(table);
        return true;
      }
    },
  };
}());
