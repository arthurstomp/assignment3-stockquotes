#Flow of the program

The program starts at `app.init(opt)` this function decide using which approach the stockquotes will be displayed. Both the Ajax and websocket approachs has a callback that updates the table. Every operation related to the interface is defined in `app.interface`.

#Concepts

###Objects, including object creation and inheritance

#####Objects in javascript
An object, in javascript, is a unordered collection of properties. Properties
can be numbers, strings, functions or objects.

```javascript
var sampleObject = {
  id : 0,
  name : "sample object",
  sayHi : function(name){
    console.log("Hi, "+name);
  },
  color : {
    hex : "#B00000",
    rgb : [176,0,0],
  },
}
```

To have access to the properties is used the dot notation.

```javascript
sampleObject.name; // "sample object"
```

To call functions is added parenthesis and parameters to the function.

```javascript
sampleObject.sayHi("dude"); // "hi, dude"

//if the function is called as a property it returns function's object

var newHi = sampleObject.sayHi;
newHi("new dude"); // "hi, new dude"
```

#####Javascript is Prototype-oriented
Javascript doesn't implement the classical object orientation. In javascript
that is no class, the behavior as inheritance or polymorphism is made using the
prototype object.

Every object has a property call prototype. The prototype is used when a
property of an object is accessed, but it doesn't exist. When that happens the
prototype is accessed and the property that didn't existed in the first object.
If the property doesn't exist in the prototype, the prototype of the prototype
is accessed, after all the prototype still an object. That goes on until
`prototype === null`

```javascript
var a = {a: 1};
// a ---> Object.prototype ---> null

var b = Object.create(a);
// b ---> a ---> Object.prototype ---> null
console.log(b.a); // 1 (inherited)

var c = Object.create(b);
// c ---> b ---> a ---> Object.prototype ---> null

var d = Object.create(null);
// d ---> null
```

#####Object creation

```javascript
var o1 = {name : "john", age: 21};

var o2 = new Object();

var o3 = Object.create(null);
```

#####Inheritance

Inheritance is possible in javascript be creating a prototype chain.

```javascript
function Angel(name){
    return {
        name : name,
        n_wings : 2,
        powers : ["flamming sword"],
        prototype : {
            constructor : Angel,
            sayHi : function(){
            console.log("I'm an Angel");
            },
        },
    };
}

function Archangel(name){
    var newAngel = Angel(name);
    newAngel.n_wings = 9;
    newAngel.powers.push("the verb");
    newAngel.prototype = {
        constructor : Archangel,
        prototype : Angel.prototype,
                        };
    return newAngel;
}

var a = Angel("Matheus");
var aa2 = Archangel("Raphael");
```

source : [Inheritance and the prototype chain](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain)

###websockets
Websocket is technology that allow developers open an interactive session between user's browser and the server.

The tool that we used to use websockets in this assignment is [socket.io](http://socket.io/).

The code to use socket.io in the client is very simple.

```javascript
var socket = io(url); // url of the server.
socket.on("stockquotes",callback); // set a callback function to respond to a "stockquotes" messages.
```

sources :
- [socket.io](http://socket.io/)
- [WebSockets](https://developer.mozilla.org/en-US/docs/WebSockets)

###XMLHttpRequest
XMLHttpRequest is a JavaScript object that provides an easy way to retrieve data from a URL without having to do a full page refresh. A Web page can update just a part of the page without disrupting what the user is doing.  XMLHttpRequest is used heavily in AJAX programming.

Despite its name, XMLHttpRequest can be used to retrieve any type of data, not just XML, and it supports protocols other than HTTP (including file and ftp).

The usage of XMLHttpRequest is simple.

```javascript
xhr=new XMLHttpRequest(); // Creates the XMLHttpRequest object.
xhr.open("GET", url); // Open a GET request to the url.
xhr.addEventListener("load", callback); // add a callback to the load event of the XMLHttpRequest to handle the response of the request.
xhr.send(); // send the request.
```

source : [XMLHttpRequest](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest)

###AJAX

AJAX(Asynchronous JavaScript + XML) is approach that uses HTML,CSS,Javascript,XML,DOM and XMLHttpRequest to make web applications update their user interface without refreshing the page.

```javascript
function loadXMLDoc()
{
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","ajax_info.txt",true);
xmlhttp.send();
}
```

sources :
- [Ajax](https://developer.mozilla.org/en/docs/AJAX)
- [AJAX Tutorial](http://www.w3schools.com/ajax/)

###Callbacks
Callbacks are functions that are passed as a parameter of other function so it can be call in the right moment.

The most common use of callback is the function passed when all HTML document have been completely loaded `document.addEventListner("DOMContentLoaded",callback)`.

To create custom callback use the principle shown in the example below.

```javascript
var sampleObject2 = {
  id : 0,
  fetchData : function(stringJSON,callback){
    var json = JSON.parse(stringJSON);
    callback(json);
  }
}

var callback = function(json){
  console.log(json)
}

sampleObject2.fetchData("[{id:1,name:\"one\"}]",callback);
```

source : [Callback (computer programming)](http://en.wikipedia.org/wiki/Callback_%28computer_programming%29)

###How to write testable code for unit-tests

To write testable code something should be avoided :

#####1. Separation of Concern :

A strong separation of concerns means that modules, objects and functions are independent from each other. In practice, strongly separated objects and functions can be tested isolated, which mean smaller and simpler tests.

#####2. Loose Coupling :

Tight coupling between components is when a component A specifically requests module B instead of exposing a mechanism where a module like B can be passed to component A. This increases testing difficulty, forcing that in the test a whole new module B needs to be created to test one functionality of module A instead of using a mock object to simulate the B module.

#####3. Eliminate Global Variables :

The use of global variables encourages states to be shared between different components, which reduces the separation of concerns.

sources :
- [Testable code best practices](http://www.sitepen.com/blog/2014/07/11/testable-code-best-practices/)
- [Writing Testable Code](http://googletesting.blogspot.nl/2008/08/by-miko-hevery-so-you-decided-to.html)
