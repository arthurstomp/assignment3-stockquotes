/*jslint browser: true, plusplus:true*/

describe("Stockquote", function () {

  it("should exists",function(){
    expect(app).toBeDefined();
  });

  it("stocks should be defined",function(){
    expect(app.stocks).toBeDefined();
  });

  it("socket should be defined",function(){
    expect(app.socket).toBeDefined();
  });

  it("socket should be connected to http://server7.tezzt.nl:1333",function(){
    expect(app.socket.io.uri).toBe("http://server7.tezzt.nl:1333");
  });

  it("stocks should be empty", function () {
    expect(app.stocks.length).toBe(0);
  });

  it("init with option 0 should execute real time data",function(){
    expect(app.init(0)).toBe(true);
  });

  it("init with option 1 should execute ajax",function(){
    expect(app.init(1)).toBe(true);
  });

  it("init with option 2 should execute with raw data",function(){
    expect(app.init(2)).toBe(true);
  });

  it("init with option >= 3 should fail",function(){
    expect(app.init(3)).toBe(false);
    expect(app.init(4)).toBe(false);
  });

  describe("settings", function(){
    it("should exist",function(){
      expect(app.settings).toBeDefined();
    });

    it("should have a valid refresh",function(){
      expect(app.settings.refresh).toEqual(jasmine.any(Number));
      expect(app.settings.refresh).toEqual(1000);
    });

    it("should have a valid ajaxUrl",function(){
      expect(app.settings.ajaxUrl).toEqual(jasmine.any(String));
      expect(app.settings.ajaxUrl).toEqual("http://server7.tezzt.nl/~theotheu/stockquotes/index.php");
    });

  });

  describe("interface.",function(){
    it("should exist",function(){
      expect(app.interface).toBeDefined();
    });

    it("should have a header",function(){
      expect(app.interface.header).toBeDefined();
    });
    it("should have a table",function(){
      expect(app.interface.table).toBeDefined();
    });
    it("greenOrRed should return true for criteria >= 0",function(){
      expect(app.interface.greenOrRed(0)).toBe(true);
    });
    it("greenOrRed should return false for criteria < 0",function(){
      expect(app.interface.greenOrRed(-1)).toBe(false);
    });
  });

});
